'use strict';

const sampleData = [
  {
    item_id: 1,
    tags_id: 1,
    createdAt: new Date(), 
    updatedAt: new Date()
  },
  {
    item_id: 1,
    tags_id: 2,
    createdAt: new Date(), 
    updatedAt: new Date()
  },
  {
    item_id: 2,
    tags_id: 2,
    createdAt: new Date(), 
    updatedAt: new Date()
  }
]

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('item_tags', sampleData, {});
  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.bulkDelete('item_tags', null, {});
  }
};
