'use strict';

let sampleCategory = [
	{ category_name: 'small fish', category_image: 'uploads/default-image-category.1.jpeg', createdAt: new Date(), updatedAt: new Date()},
	{ category_name: 'medium fish', category_image: 'uploads/default-image-category.2.jpeg',  createdAt: new Date(), updatedAt: new Date()},
	{ category_name: 'large fish', category_image: 'uploads/default-image-category.3.jpeg', createdAt: new Date(), updatedAt: new Date()},
	{ category_name: 'fancy fish', category_image: 'uploads/default-image-category.4.jpeg', createdAt: new Date(), updatedAt: new Date()},
	{ category_name: 'colorful fish', category_image: 'uploads/default-image-category.5.jpeg', createdAt: new Date(), updatedAt: new Date()}
];

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('categories', sampleCategory, {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('categories', null, {});
	}
};