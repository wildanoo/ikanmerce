'use strict';

const sampleData = [
  {item_id : 1, image_name:"image 1", image : "uploads/default-image-carousel.1.jpeg", createdAt: new Date(), updatedAt: new Date()},
  {item_id : 1, image_name:"image 2", image : "uploads/default-image-carousel.2.jpeg", createdAt: new Date(), updatedAt: new Date()},
  {item_id : 2, image_name:"image 3", image : "uploads/default-image-carousel.3.jpeg", createdAt: new Date(), updatedAt: new Date()}
]

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('item_images', sampleData, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('item_images', null, {});
  }
};
