'use strict';

const dataTest = [{
  invoice_id: 1,
  recipient_name: "wildan",
  phone: "082112341234",
  address: "perumahan setra asri no 32 cimahi utara",
  city: "cimahi",
  province: "Jawa Barat",
  createdAt: new Date(),
  updatedAt: new Date()
},
{
  invoice_id: 2,
  recipient_name: "pandu",
  phone: "082112341239",
  address: "perumahan setra duta no 10 cimahi utara",
  city: "cimahi",
  province: "Jawa Barat",
  createdAt: new Date(),
  updatedAt: new Date()
}
]

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    return queryInterface.bulkInsert('shippingAddrs',dataTest , {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('shippingsAddrs', null, {});
  }
};
