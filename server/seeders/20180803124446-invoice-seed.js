'use strict';

let data = [
  {user_id : 1, cart_number : 1, total_price : 300000, status: true, createdAt: new Date(), updatedAt: new Date()},
  {user_id : 2, cart_number : 1, total_price : 200000, status: false, createdAt: new Date(), updatedAt: new Date()}
];

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('invoices', data, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('invoices', null, {});
  }
};
