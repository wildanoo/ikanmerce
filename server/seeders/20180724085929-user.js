'use strict';

require('dotenv').config();
const bcrypt = require('bcrypt');

//dummy

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let dataUser = [
      {
        name: 'admin',
        email: 'admin@gmail.com',
        phone: '082112341234',
        address: 'jl. raya bojongsoang 145/a',
        gender: 'pria',
        password: await bcrypt.hash(process.env.BCRYPT_PASS, parseInt(process.env.BCRYPT_SALT)),
        is_admin: true,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'budi',
        email: 'budi@gmail.com',
        phone: '0821122233344',
        address: 'jl. raya setra duta 15',
        gender: 'pria',
        password: await bcrypt.hash(process.env.BCRYPT_PASS, parseInt(process.env.BCRYPT_SALT)),
        is_admin: false,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ];
      return queryInterface.bulkInsert('users', dataUser, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('users', null, {});
  }
};
