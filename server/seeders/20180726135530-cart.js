'use strict';

const data = [
  {
    user_id:'1',
    item_id:'1',
    item_detail_id:'1',
    quantity:'2',
    cart_number:'1',
    status: true,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    user_id:'2',
    item_id:'1',
    item_detail_id:'2',
    quantity:'3',
    cart_number:'1',
    status: true,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    user_id:'2',
    item_id:'2',
    item_detail_id:'3',
    quantity:'2',
    cart_number:'1',
    status: true,
    createdAt: new Date(),
    updatedAt: new Date()
  },
]

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('carts', data, {});
  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.bulkDelete('carts', null, {});
  }
};
