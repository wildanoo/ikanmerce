'use strict';

let sampleItem = [
  { category_id: 1 , 
    item_name: 'dory', 
    description: 'small fish and grouping fish',
    specification: 'colorful body',
    featured_image_name: 'dory fish',
    featured_image: 'uploads/default-image.jpeg',
    createdAt: new Date(), 
    updatedAt: new Date()
  },
	{ category_id: 2 , 
    item_name: 'nemo', 
    description: 'medium fish good for small aquarium',
    specification: 'colorful body',
    featured_image_name: 'nemo fish',
    featured_image: 'uploads/default-image.1.jpeg',
    createdAt: new Date(), 
    updatedAt: new Date()
  }
];

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('items', sampleItem, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('items', null, {});
  }
};
