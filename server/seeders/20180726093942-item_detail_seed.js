'use strict';

let sampleItemDetail = [
  { item_id: 1 , 
    size: 'small', 
    price: 50000,
    stock: 50,
    discount: 2.5,
    createdAt: new Date(), 
    updatedAt: new Date()
  },
	{ item_id: 1 , 
    size: 'medium', 
    price: 75000,
    stock: 30,
    discount: 4,
    createdAt: new Date(), 
    updatedAt: new Date()
  },
  { item_id: 2 , 
    size: 'medium', 
    price: 60000,
    stock: 20,
    discount: 0,
    createdAt: new Date(), 
    updatedAt: new Date()
  },
];

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('item_details', sampleItemDetail, {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('item_details', null, {});
  }
};
