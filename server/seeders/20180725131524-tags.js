'use strict';

let sampleData = [
  {
    tag_name: 'cute fish',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    tag_name: 'aquascape fish',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    tag_name: 'angry fish',
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    tag_name: 'tank fish',
    createdAt: new Date(),
    updatedAt: new Date()
  }
];

module.exports = {
  up: (queryInterface, Sequelize) => {

      return queryInterface.bulkInsert('tags', sampleData, {});
  },

  down: (queryInterface, Sequelize) => {

      return queryInterface.bulkDelete('tags', null, {});
  }
};
