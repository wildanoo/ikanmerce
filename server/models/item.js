'use strict';
module.exports = (sequelize, DataTypes) => {
  var item = sequelize.define('item', {
    category_id: DataTypes.INTEGER,
    item_name: DataTypes.STRING,
    description: DataTypes.STRING,
    specification: DataTypes.STRING,
    featured_image_name: DataTypes.STRING,
    featured_image: DataTypes.STRING
  }, {});
  item.associate = function(models) {
    item.belongsTo(models.category,{
      foreignKey : 'category_id'
    }),
    item.hasMany(models.item_detail, {
      foreignKey : 'item_id',
      as : 'item_details'
    }),
    item.hasMany(models.item_images, {
      foreignKey : 'item_id',
      as : 'item_images'
    }),
    item.belongsToMany(models.tags,{
      through : 'item_tags',
      foreignKey : 'item_id',
      as : 'tags'
    }),
    item.hasMany(models.item_tags,{
      foreignKey : 'item_id',
      as : 'item_tag'
    })
  };
  return item;
};