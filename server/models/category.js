'use strict';
module.exports = (sequelize, DataTypes) => {
  var category = sequelize.define('category', {
    category_name: {
      type : DataTypes.STRING,
      allowNull: false
    },
    category_image : {
      type : DataTypes.STRING,
    }
  }, {});
  category.associate = function(models) {
      category.hasMany(models.item,{
        foreignKey: 'category_id',
        as: 'items'
      })
  };
  return category;
};