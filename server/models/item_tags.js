'use strict';
module.exports = (sequelize, DataTypes) => {
  var item_tags = sequelize.define('item_tags', {
    item_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    tags_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {});
  item_tags.associate = function(models) {
    item_tags.belongsTo(models.item,{
      foreignKey : 'item_id',
      onDelete : 'cascade'
    }),
    item_tags.belongsTo(models.tags,{
      foreignKey : 'tags_id',
      onDelete : 'cascade'
    })
  };
  return item_tags;
};