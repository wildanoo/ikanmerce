'use strict';
module.exports = (sequelize, DataTypes) => {
  var item_images = sequelize.define('item_images', {
    item_id: DataTypes.INTEGER,
    image_name: DataTypes.STRING,
    image: DataTypes.STRING
  }, {});
  item_images.associate = function(models) {
    item_images.belongsTo(models.item,{
      foreignKey : 'item_id',
      onDelete : 'cascade'
    })
  };
  return item_images;
};