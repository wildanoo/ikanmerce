'use strict';
module.exports = (sequelize, DataTypes) => {
  var tagsModel = sequelize.define('tags', {
    tag_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  }, {});
  tagsModel.associate = function(models) {
    tagsModel.belongsToMany(models.item,{
      through : 'item_tags',
      foreignKey : 'tags_id',
      as : 'items'
    }),
    tagsModel.hasMany(models.item_tags,{
      foreignKey : 'tags_id',
      as : 'item_tag'
    })
  };
  return tagsModel;
};