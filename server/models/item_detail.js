'use strict';
module.exports = (sequelize, DataTypes) => {
  var item_detail = sequelize.define('item_detail', {
    item_id: DataTypes.INTEGER,
    size: DataTypes.STRING,
    price: DataTypes.INTEGER,
    stock: DataTypes.INTEGER,
    discount: DataTypes.DECIMAL
  }, {});
  item_detail.associate = function(models) {
    item_detail.belongsTo(models.item,{
      foreignKey : 'item_id',
      onDelete : 'cascade'
    })
  };
  return item_detail;
};