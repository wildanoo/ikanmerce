'use strict';
module.exports = (sequelize, DataTypes) => {
  var cart = sequelize.define('cart', {
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    item_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    item_detail_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cart_number: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {});
  cart.associate = function(models) {
    cart.hasMany(models.item, {
      foreignKey: "id",
      sourceKey: "item_id"
    }),
    cart.hasMany(models.item_detail, {
      foreignKey: "id",
      sourceKey: "item_detail_id"
    }),
    cart.hasMany(models.user, {
      foreignKey: "id",
      sourceKey: "user_id"
    }),
    cart.belongsTo(models.invoice, {
      foreignKey: "cart_number",
      sourceKey: 'cart_number'
    })
  };
  return cart;
};