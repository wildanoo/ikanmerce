'use strict';
module.exports = (sequelize, DataTypes) => {
  var shippingAddr = sequelize.define('shippingAddr', {
    invoice_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    recipient_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: {
          args: [10, 12],
          msg: "Phone number digit should around (10 - 12)"
        },
        not: {
          args: ["[a-z]",'i'],
          msg: "Phone number is invalid"
        } 
      }
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false
    },
    province: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  shippingAddr.associate = function(models) {
    shippingAddr.belongsTo(models.invoice,{
      foreignKey: "invoice_id",
      as: 'shipping_address'
    })
  };
  return shippingAddr;
};