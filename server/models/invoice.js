'use strict';
module.exports = (sequelize, DataTypes) => {
  var invoice = sequelize.define('invoice', {
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    cart_number: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    total_price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    payment_link: {
      type: DataTypes.STRING
    }
  }, {});
  invoice.associate = function(models) {
    invoice.belongsTo(models.user, {
      foreignKey: "user_id"
    }),
    invoice.hasMany(models.cart, {
      foreignKey: "cart_number",
      sourceKey: "cart_number"
    }),
    invoice.hasOne(models.shippingAddr, {
      foreignKey: "invoice_id",
      as: 'shipping_address'
    })
  };
  return invoice;
};