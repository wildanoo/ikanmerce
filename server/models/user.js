'use strict';
module.exports = (sequelize, DataTypes) => {
  var user = sequelize.define('user', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: "Invalid email!"
        },
        notEmpty: {
          msg: "Email tidak boleh kosong!"
        } 
      }
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        len: {
          args: [10, 12],
          msg: "Jumlah karakter Phone Number tidak memenuhi (10 - 12)"
        },
        not: {
          args: ["[a-z]",'i'],
          msg: "Phone number is invalid"
        } 
      }
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
      isIn: {
        args: [['pria', 'wanita']],
        msg: "Anda waria"
      }
      
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    is_admin: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {});
  user.associate = function(models) {
    user.hasMany(models.cart, {
      foreignKey: 'user_id',
      as: 'cart'
    })
  };
  return user;
};