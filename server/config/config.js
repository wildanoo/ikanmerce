require('dotenv').config();
const Sequelize = require("sequelize");
module.exports = 
{
  "development": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": process.env.DB_NAME,
    "host": "127.0.0.1",
    "port": 5432,
    "dialect": "postgres",
    "operatorsAliases": Sequelize.Op

  },
  "test": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": "ikanmerce_test",
    "host": "127.0.0.1",
    "port":5432,
    "dialect": "postgres",
    "operatorsAliases": Sequelize.Op
  },
  "production": {
    "username": process.env.PROD_DB_USER,
    "password": process.env.PROD_DB_PASS,
    "database": process.env.PROD_DB_NAME,
    "host": process.env.PROD_DB_HOST,
    "port":5432,
    "dialect":"postgres",
    "operatorsAliases": Sequelize.Op,
    "ssl": true,
    "dialectOptions": {
        "ssl": true
    }
  }
}
