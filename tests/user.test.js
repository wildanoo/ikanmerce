const user = require('./user');

describe("Testing all user endpoint", () => {

    test('Register new user',() => {
        expect.assertions(3)
        return user.registerUser().then(user => {
            expect(user).toBeDefined();
            expect(user.data.success).toBe(true);
            expect(user.status).toBe(200);
        })
    })

    test('Login user', () => {
        expect.assertions(3)
        return user.loginUser().then(user => {
            expect(user).toBeDefined();
            expect(user.data.success).toBe(true);
            expect(user.status).toBe(200);
        })
    })

})