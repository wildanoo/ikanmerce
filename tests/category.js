const axios = require('axios');
const fs = require('fs');
const FormData = require('form-data')
require('dotenv').config();
const authTokenLogin = process.env.BEARER_AUTH;

const cat = {

    fetchOneCategory: () => {
        return axios
        .get('http://localhost:3000/api/categories/1')
        .then(res => { return {data: res.data, status: res.status }})
        .catch(err => err.message);
    },
    fecthAllCategory: () => {
        return axios
        .get('http://localhost:3000/api/categories/')
        .then(res => { return {data: res.data, status: res.status }})
        .catch(err => err.message);
    },

    createNewCategory : () => {
        let data = new FormData();
        data.append('category_image', fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg');
        data.append('category_name', 'Image ke 1')

        let options = {
            method: 'POST',
            url: 'http://localhost:3000/api/categories',
            headers: {
                'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                'Authorization': authTokenLogin,
            },
            data
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => err.message);
    },

    editCategory: () => {
        let data = new FormData();
        data.append('category_image', fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg');
        data.append('category_name', 'Image ke 1 edited')

        let options = {
            method: 'PUT',
            url: 'http://localhost:3000/api/categories/6',
            headers: {
                'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                'Authorization': authTokenLogin,
            },
            data
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => err.message);
    },

    removeCategory: () => {
        let options = {
            method: 'DELETE',
            url: 'http://localhost:3000/api/categories/6',
            headers: {
                'Content-Type': `application/json`,
                'Authorization': authTokenLogin,
            }
        }

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => err.message);
    }


    
}

module.exports = cat