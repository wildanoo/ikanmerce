const axios = require('axios')
const FormData = require('form-data')
const fs = require('fs')
require('dotenv').config();
const authTokenLogin = process.env.BEARER_AUTH;

const item = {

    createNewItem : () => {

        let itemDetail = [{"item_id":"1","size":"medium", "price":"20000","stock":"20","discount":"2.5"},
        {"item_id":"1","size":"small", "price":"10000","stock":"30","discount":"0"}];
        let imageName = ['images ke 1','images ke 2','images ke 3','images ke 4'];
        let tags = ['mini fish','cheap fish','expensive fish','colorful fish'];
        
        let data = new FormData();
        data.append('category_id','1')
        data.append('item_name','Ikan koi putih test')
        data.append('description','Description of ikan koi putih')
        data.append('specification','Specification of ikan koi putih')
        data.append('featured_image', fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg');
        data.append('featured_image_name', 'Featured image test')

        for (var i = 0; i < itemDetail.length; i++) {
            for ( var key in itemDetail[i] ) {
                data.append(`item_details[${i}][${key}]`, itemDetail[i][key])
            }
        }

        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')
        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')
        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')
        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')

        for (var i = 0; i < imageName.length; i++ ){
            data.append(`image_name[${i}]`,imageName[i])
        }
        for (var i = 0; i < tags.length; i++ ){
            data.append(`tags[${i}]`,tags[i])
        }

        let options = {
            method: 'POST',
            url: 'http://localhost:3000/api/items/',
            headers: {
                'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                'Authorization': authTokenLogin,
            },
            data
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => err.message);
    },

    updateItem : () => {
        let itemDetail = [{"id":4,"item_id":"1","size":"changed", "price":"20000","stock":"20","discount":"2.5"},
        {"id":5,"item_id":"1","size":"small", "price":"10000","stock":"30","discount":"0"}];
        let imageName = ['images ke 1 edited','images ke 2','images ke 3','images ke 4'];
        let tags = ['mini fish','cheap fish','expensive fish','colorful fish'];
        let imageId = [4,5,6,7];
        
        let data = new FormData();
        data.append('category_id','1')
        data.append('item_name','dory testing 2')
        data.append('description','Description of ikan koi putih')
        data.append('specification','Specification of ikan koi putih')
        data.append('featured_image', fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg');
        data.append('featured_image_name', 'Featured image test')

        for (var i = 0; i < itemDetail.length; i++) {
            for ( var key in itemDetail[i] ) {
                data.append(`item_details[${i}][${key}]`, itemDetail[i][key])
            }
        }

        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')
        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')
        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')
        data.append('images',fs.createReadStream('./public/uploads/default-image.jpeg'), 'default-image.jpeg')

        for (var i = 0; i < imageId.length; i++ ){
            data.append(`images_id[${i}]`,imageId[i])
        }
        for (var i = 0; i < imageName.length; i++ ){
            data.append(`image_name[${i}]`,imageName[i])
        }
        for (var i = 0; i < tags.length; i++ ){
            data.append(`tags[${i}]`,tags[i])
        }

        let options = {
            method: 'PATCH',
            url: 'http://localhost:3000/api/items/3',
            headers: {
                'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
                'Authorization': authTokenLogin,
            },
            data
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => err.message);
    },

    fecthAllItem: () => {
        return axios
        .get('http://localhost:3000/api/items/')
        .then(res => { return {data: res.data, status: res.status }})
        .catch(err => err.message)
    },

    fetchOneItem: () => {
        return axios
        .get('http://localhost:3000/api/items/3')
        .then(res => { return {data: res.data, status: res.status }})
        .catch(err => err.message)
    },

    removeOneItem : () => {
        let options = {
            method: 'DELETE',
            url: 'http://localhost:3000/api/items/3',
            headers: {
                'Content-Type': `application/json`,
                'Authorization': authTokenLogin,
            }
        }

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => console.log(err.message))
    }
    

}

module.exports = item