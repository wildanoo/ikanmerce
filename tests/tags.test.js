const tag = require('./tags');

describe('Testing all tags endpoint', () => {

    test('Create a new tag', () => {
        expect.assertions(2)
        return tag.createNewTags().then(tag => {
            expect(tag).toBeDefined();
            expect(tag.status).toEqual(201);
        })
    })

    test('Edit a tag', () => {
        expect.assertions(2)
        return tag.updateTag().then(tag => {
            expect(tag).toBeDefined();
            expect(tag.status).toEqual(200);
        })
    })

    test('Get all tags', () => {
        expect.assertions(2)
        return tag.fetchTag().then(tag => {
            expect(tag).toBeDefined();
            expect(tag.status).toEqual(200);
        })
    })

    test('Remove a tag', () => {
        expect.assertions(2)
        return tag.removeTag().then(tag => {
            expect(tag).toBeDefined();
            expect(tag.status).toEqual(200);
        })
    })

})