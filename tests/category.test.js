const cat = require('./category');

describe('Testing all category endpoint', () => {

    test("Create new category", () => {
        expect.assertions(3)
        return cat.createNewCategory().then(category => {
            expect(category).toBeDefined();
            expect(category.data.success).toBe(true);
            expect(category.status).toEqual(201);
        })
    })

    test("Fetching 1 category", () => {
        expect.assertions(3);
        return cat.fetchOneCategory().then(category => {
            expect(category).toBeDefined();
            expect(category.data.success).toBe(true);
            expect(category.status).toEqual(200);
        })
    })

    test("Fetching all category", () => {
        expect.assertions(3)
        return cat.fecthAllCategory().then(category => {
            expect(category).toBeDefined();
            expect(category.data.success).toBe(true);
            expect(category.status).toEqual(200);
        })
    })

    test("Updating category", () => {
        expect.assertions(3)
        return cat.editCategory().then(category => {
            expect(category).toBeDefined();
            expect(category.data.success).toBe(true);
            expect(category.status).toEqual(200);
        })
    })

    test("Remove category", () => {
        expect.assertions(3)
        return cat.removeCategory().then(category => {
            expect(category).toBeDefined();
            expect(category.data.success).toBe(true);
            expect(category.status).toEqual(200);
        })
    })



})
