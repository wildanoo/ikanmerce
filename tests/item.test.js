const item = require('./item')

describe("Testing all item endpoint", () => {

    test('Create new item', () => {
        expect.assertions(3)
        return item.createNewItem().then(item => {
            expect(item).toBeDefined();
            expect(item.data.success).toBe(true)
            expect(item.status).toBe(200)
        })
    })

    test('Update an item', () => {
        expect.assertions(3)
        return item.updateItem().then(item => {
            expect(item).toBeDefined();
            expect(item.data.success).toBe(true)
            expect(item.status).toBe(200)
        })
    })

    test('Get one item', () => {
        expect.assertions(3)
        return item.fetchOneItem().then(item => {
            expect(item).toBeDefined();
            expect(item.data.success).toBe(true)
            expect(item.status).toBe(200)
        })
    })

    test('Get all item', () => {
        expect.assertions(3)
        return item.fecthAllItem().then(item => {
            expect(item).toBeDefined();
            expect(item.data.success).toBe(true)
            expect(item.status).toBe(200)
        })
    })

    test('Remove one item', () => {
        expect.assertions(3)
        return item.removeOneItem().then(item => {
            expect(item).toBeDefined();
            expect(item.data.success).toBe(true)
            expect(item.status).toBe(200)
        })
    })



})