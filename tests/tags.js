const axios = require('axios')
require('dotenv').config();
const authTokenLogin = process.env.BEARER_AUTH;

const tags = {

    createNewTags : () => {
        let data = {
            tagItems : "Tag for test"
        }
        let options = {
            method: 'POST',
            url: 'http://localhost:3000/api/tags/',
            headers: {
                'Content-Type': `application/json`,
                'Authorization': authTokenLogin,
            },
            data
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => console.log('error'));
    },

    updateTag : () => {
        let data = {
            tagItems : "Tag updated"
        }
        let options = {
            method: 'PUT',
            url: 'http://localhost:3000/api/tags/5',
            headers: {
                'Content-Type': `application/json`,
                'Authorization': authTokenLogin,
            },
            data
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => console.log('error'));
    },

    fetchTag : () => {
        let options = {
            method: 'GET',
            url: 'http://localhost:3000/api/tags'
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => console.log('error'));
    },

    removeTag : () => {
        let options = {
            method: 'DELETE',
            url: 'http://localhost:3000/api/tags/5',
            headers: {
                'Content-Type': `application/json`,
                'Authorization': authTokenLogin,
            },
        };

        return axios(options)
            .then(res => { return {data: res.data, status: res.status }})
            .catch(err => console.log('error'));
    }



}

module.exports = tags