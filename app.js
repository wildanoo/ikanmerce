const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const rateLimit = require('express-rate-limit');

const passport = require('passport');
require('./config/passport');
const flash = require('connect-flash');
// const session   = require('express-session');

const usersRouter = require('./routes/users');
const categoryRouter = require('./routes/category');
const tagsRouter = require('./routes/tags');
const itemsRouter = require('./routes/items');
const cartRouter = require('./routes/cart');
const invoiceRouter = require('./routes/invoice');

const app = express();

app.enable('trust proxy');
const limiter = new rateLimit({
    windowMs: 15*60*1000, 
    max: 100,
    delayMs: 0
})
app.use(limiter)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
// app.use(session({
//     secret: 'secret cat',
//     resave: true,
//     saveUninitialized: true
// }));

app.use(cors())

// app.use(session({ secret: 'testingpassport'}))
app.use(passport.initialize());
// app.use(passport.session());

app.use(logger('dev'));
app.use(flash());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/users', usersRouter);
app.use('/api/categories', categoryRouter);
app.use('/api/tags', tagsRouter);
app.use('/api/items', itemsRouter);
app.use('/api/cart', cartRouter);
app.use('/api/invoice', invoiceRouter)

app.use('/api', (req,res) => res.status(200).send("Welcome to ikanmerce API"))

module.exports = app;
