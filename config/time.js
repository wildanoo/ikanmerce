let time = function (x) {
    let greenTime = x;
    let hours = (greenTime.getHours());
    let minutes = (greenTime.getMinutes());
    let second = (greenTime.getSeconds())
    let year = (greenTime.getFullYear())
    let day = (greenTime.getUTCDate())
    let month = (greenTime.getUTCMonth() + 1)

    let jam = 0
    let menit = 1
    let detik = 0
    let hari = 0
    let bulan = 0
    let zone = "+0700"
    let allDay = '0-6'
    if (minutes < 10) {
        menit = "0" + minutes
    } else {
        menit += minutes
    }
    if (hours < 10) {
        jam = "0" + hours
    } else {
        jam += hours
    }
    if (second < 10) {
        detik = "0" + second
    } else {
        detik += second
    }
    if (day < 10) {
        hari = "0" + day
    } else {
        hari += day
    }
    if (month < 10) {
        bulan = "0" + month
    } else {
        bulan += month
    }
    let total = jam + ":" + menit
    let times = year + "-" + bulan + "-" + hari + " " + total + ":" + detik + " " + zone;
    return time = times
}

let timeCron = function (x) {
    let greenTime = x;
    let hours = (greenTime.getHours());
    let minutes = (greenTime.getMinutes());
    let second = (greenTime.getSeconds())

    let jam = 0
    let menit = 1
    let detik = 0
    if (minutes < 10) {
        menit = "0" + minutes
    } else {
        menit += minutes
    }
    if (hours < 10) {
        jam = "0" + hours
    } else {
        jam += hours
    }
    if (second < 10) {
        detik = "0" + second
    } else {
        detik += second
    }
    let totalNew = detik + " " + menit + " " + jam
    return time = totalNew
}

module.exports = {
    time,
    timeCron
}