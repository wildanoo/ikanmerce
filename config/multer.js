const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/uploads')
    },
    filename: (req, file, cb) => {
        let selectExt = file.originalname.substr( file.originalname.lastIndexOf('.'));
        let front = file.originalname.replace(selectExt,"");
        let ext = file.originalname.substr( file.originalname.lastIndexOf('.') + 1);
        let random = Math.random().toString(36).slice(8);

        cb(null, front + '-' + Date.now() +'-'+ random + '.' + ext )
    }
});
const fileFilter = (req, file, cb) => {
    let mime = file.mimetype;
    if(mime === 'image/jpeg' || mime === 'image/png' ){
        cb(null, true)
    }else{
        cb(null, false)
    }
}
const limits = {
    fieldSize: 5 * 1024 * 1024
}
const upload = multer({
    storage,
    fileFilter,
    limits
})

module.exports = upload