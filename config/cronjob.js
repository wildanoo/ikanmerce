const axios = require('axios');
const CronJob = require('cron').CronJob;
const invoice1 = require('../server/models').invoice;
const encode = require('nodejs-base64-encode');
const tokenSandbox = "SB-Mid-server-0xFGNOfVz_j_dkYZGXS9BYiz"
const AUTH_TOKEN = encode.encode(tokenSandbox, 'base64')
const invoice = require('../server/models').invoice;

axios.defaults.headers.common['Authorization'] = 'Basic ' + AUTH_TOKEN;
axios.defaults.headers.get['Accept'] = 'application/json';

// let cekStatusPayment = async function () {
//     let job = new CronJob({
//         cronTime: '* */1 * * * *',
//         onTick: function () {

//         },
//         start: true,
//         timeZone: 'Asia/Jakarta'
//     })
// }

// let sendEmail = async function () {
//     let job = new CronJob({
//         cronTime: `${time.timeCron()} * * 0-6`,
//         onTick: function () {
//             //cek database status
//             //if
//             let transporter = await nodemailer.createTransport({
//                 host: "smtp.mailtrap.io",
//                 port: 2525,
//                 auth: {
//                     user: "456c664af2e8d4",
//                     pass: "c9d25036ce1a59"
//                 }
//             })

//             let mailOptions = {
//                 from: '"Para Pencari Ikan" <panduakas@gmail.com>',
//                 to: req.user.email,
//                 subject: `Invoice Payment Ikan`,
//                 text: `To complete the payment, click this following link ${response.data.redirect_url}`
//             }

//             transporter.sendMail(mailOptions, (error, info) => {
//                 if (error) {
//                     // return res.send(error)                                            
//                     console.log(error);
//                 }
//                 console.log("message was sent!");
//                 console.log(info);

//             })
//         },
//         start: false,
//         timeZone: 'Asia/Jakarta'
//     })
// }

// let sendEmailAuto = async function () {

// }

let checkStatus = async function () {
    let dataUserInvoiceArray = []
    // let checkDatabase = new CronJob({
    //     cronTime: '* * * * *',
    //     onTick: async function () {
    //         let dataUserInvoice = await invoice1.findAll({
    //             where: {
    //                 status: true
    //             }
    //         })
    //         for (let i = 0; dataUserInvoice.length > i; i++) {
    //             dataUserInvoiceArray.push(dataUserInvoice[i])
    //             console.log(dataUserInvoiceArray[i].payment_link);
    //         }
    //     },
    //     start: true,
    //     timeZone: 'Asia/Jakarta'
    // })

    let jobCheckStatus = new CronJob({
        cronTime: '10 * * * * *',
        onTick: async function () {
            let dataUserInvoice = await invoice1.findAll({
                where: {
                    status: true
                }
            })
            // for (let i = 0; dataUserInvoice.length > i; i++) {
            //     dataUserInvoiceArray.push(dataUserInvoice[i])
            //     // console.log(dataUserInvoiceArray[i].payment_link);
            //     console.log('-------------'+ dataUserInvoiceArray.length);
            // }
            // console.log(dataUserInvoiceArray);
            for (const data of dataUserInvoice) {
                if (data.id) {
                    let urlGet = `https://api.sandbox.midtrans.com/v2/ORDER-${data.id}/status`
                    let response = await axios({
                        method: 'get',
                        url: urlGet,
                    })
                    console.log(response);
                    if (response.data.transaction_status === 'pending') {
                        console.log('jalan');
                        console.log(data.id);
                        await data.update({
                            where: {
                                id: data.id
                            },
                            status: false,
                            payment_link: 'payment link doesnt exist'
                        })
                    }
                }
            }


            // for (let i = 0; dataUserInvoice.length > i; i++) {
            //     if (dataUserInvoice.length === 0) {
            //         console.log(dataUserInvoice.length);
            //     } else {
                    
            //     }

            // }


        },
        start: true,
        timeZone: 'Asia/Jakarta'
    })


    //     console.log(dataUserInvoice[i].status);
    //     if (dataUserInvoice[i].status === true) {
    //         jobCheckStatus.start()
    //     } else {
    //         jobCheckStatus.stop()
    //     }

}

module.exports = {
    // sendEmail,
    // sendEmailAuto,
    checkStatus
}