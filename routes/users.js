const express = require('express');
const router = express.Router();
const passportControllers = require('../controller/passport');
const registerControllers = require('../controller/register');

router.post('/login', passportControllers.login);
router.get('/check',passportControllers.checkCurrentAuth);
router.post('/register', registerControllers.register)

module.exports = router;
