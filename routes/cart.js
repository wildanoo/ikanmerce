const express = require('express');
const router = express.Router();
const cartControllers = require('../controller/cart');
const passportControllers = require('../controller/passport');

router.get('/', passportControllers.checkAuth, cartControllers.getAllCart)
router.post('/', passportControllers.checkAuth, cartControllers.checkOut)
router.delete('/:cartNum', passportControllers.checkAuth, cartControllers.deleteCart)

module.exports = router;