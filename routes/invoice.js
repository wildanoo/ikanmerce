const express = require('express');
const router = express.Router();
const invoceControllers = require('../controller/invoice');
const passportControllers = require('../controller/passport');

// router.get('/', passportControllers.checkAuth, invoceControllers.sendEmailInvoice)
router.post('/', passportControllers.checkAuth, invoceControllers.invoiceCreate)
router.post('/getstatus', invoceControllers.getStatusPayment)


module.exports = router;