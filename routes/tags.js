const express = require('express');
const router = express.Router();
const tagsControllers = require('../controller/tags');
const passportControllers = require('../controller/passport');

router.get('/', tagsControllers.getAll);
router.post('/', passportControllers.checkAuth, tagsControllers.addTags);
router.delete('/:tagsId', passportControllers.checkAuth, tagsControllers.removeTags);
router.put('/:tagsId',passportControllers.checkAuth, tagsControllers.updateTags );

module.exports = router;
