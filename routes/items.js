const express = require('express');
const router = express.Router();
const upload = require('../config/multer');
const cpUpload = upload.fields([{ name: 'featured_image', maxCount: 1 }, { name: 'images', maxCount: 4 }])

const passportControllers = require('../controller/passport');
const itemsControllers = require('../controller/items');

router.post('/', passportControllers.checkAuth, cpUpload, itemsControllers.insertItemDetails)
router.get('/:itemId?', itemsControllers.getItems);
router.patch('/:itemId', passportControllers.checkAuth,cpUpload, itemsControllers.updateItem );
router.delete('/:itemId', passportControllers.checkAuth, itemsControllers.removeItem);

module.exports = router;