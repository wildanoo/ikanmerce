const express = require('express');
const router = express.Router();
const upload = require('../config/multer');
const cpUpload = upload.fields([{ name: 'category_image', maxCount: 1 }])
const categoryControllers = require('../controller/category');
const passportControllers = require('../controller/passport');

router.get('/:categoryId?', categoryControllers.getAll);
router.post('/', passportControllers.checkAuth, cpUpload, categoryControllers.create);
router.delete('/:categoryId', passportControllers.checkAuth, categoryControllers.remove);
router.put('/:categoryId',passportControllers.checkAuth, cpUpload,categoryControllers.update );

module.exports = router;