const fs = require('fs');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const item = require('../server/models').item;
const item_detail = require('../server/models').item_detail;
const category = require('../server/models').category;
const item_images = require('../server/models').item_images;
const tags = require('../server/models').tags;
const item_tags = require('../server/models').item_tags;

// still have bugs, when errors occured when inser/update with files, the files not deleted.

let insertItemDetails = async (req, res, next) => {
    try {
        if (!req.user.is_admin) {
            throw new Error("User is not an admin")
        }

        let queryCheck = await item.findOne({
            where: {
                item_name: req.body.item_name.trim().toLowerCase()
            }
        })

        if (queryCheck) {
            throw new Error("Item name already exist")
        }

        if(!req.body.item_details){
            throw new Error("Item details required")
        }

        console.log('KKKKK : ', req.body);

        let itemData = {
            category_id: req.body.category_id,
            item_name: req.body.item_name.trim().toLowerCase(),
            description: req.body.description.trim(),
            specification: req.body.specification.trim(),
            featured_image_name: req.body.featured_image_name.trim(),
            featured_image : 'uploads/default-image.jpeg'
        }
        if(req.files.featured_image){
            itemData.featured_image = "uploads/"+req.files.featured_image[0].filename
        }

        let insertItem = await item.create(itemData);

        if (req.body.item_details && req.body.item_details.length > 0) {
            // change map to for, or use promise.all()
            await Promise.all( req.body.item_details.map(async (x) => {
                    let parsed = typeof x === 'string' ? JSON.parse(x) : x
                    await item_detail.create({
                        item_id: insertItem.id,
                        size: parsed.size.trim(),
                        price: parsed.price,
                        stock: parsed.stock,
                        discount: parsed.discount
                    })
                })
            )
        }

        if (req.files.images && req.files.images.length > 0) {
            await Promise.all(req.files.images.map(async (x, i) => {
                    await item_images.create({
                        item_id: insertItem.id,
                        image_name: req.body.image_name[i],
                        image: "uploads/"+x.filename
                    })
                })
            )
        }

        if (req.body.tags && req.body.tags.length > 0) {

            let trimmed = req.body.tags.map((x,i) => {
                return x.trim().toLowerCase()
            });

            await Promise.all(trimmed.map(async (x, i) => {

                let tagQuery = await tags.findOne({
                    where : {tag_name : x }
                })

                if(!tagQuery){

                    let insertTag = await tags.create({
                        tag_name: x,
                    })

                    await item_tags.create({
                        item_id: insertItem.id,
                        tags_id: insertTag.id
                    })
                }else{
                    await item_tags.create({
                        item_id: insertItem.id,
                        tags_id: tagQuery.id
                    })
                }
                
            }))
        }

        return res.status(200).json({
            success: true,
            message: "Item successfully created",
        })

    } catch (error) {
        return res.status(400).send({
            success: false,
            message: error.message
        })
    }
}

let updateItem = async (req, res, next) => {
    try {
        if (!req.user.is_admin) {
            throw new Error('User is not an admin')
        }

        let queryCheck = await item.findOne({
            include: [
                {model: item_images , as: 'item_images'},
                {model: item_detail, as : 'item_details'},
                {model: category, as: 'category'},
                {model: tags, as: 'tags'}
            ],
            where: {
                id: req.params.itemId
            }
        })

        if (!queryCheck) {
            throw new Error('Item does not exist')
        }

        let editedNameCheck = await item.findOne({
            where: {
                item_name: req.body.item_name.trim().toLowerCase()
            }
        })

        if(editedNameCheck){
            throw new Error('Item name is taken')
        }

        let itemData = {
            category_id: req.body.category_id,
            item_name: req.body.item_name.trim().toLowerCase(),
            description: req.body.description.trim(),
            specification: req.body.specification.trim(),
            featured_image_name: req.body.featured_image_name.trim()
        }

        if(req.files.featured_image){
            itemData.featured_image = "uploads/"+req.files.featured_image[0].filename
        }else{
            itemData.featured_image = queryCheck.featured_image
        }

        let checkFile = await existPromise(queryCheck.featured_image)

        if(checkFile && req.files.featured_image){
            await unlinkPromise(queryCheck.featured_image);
        }

        await item.update(itemData,{ where: { id: queryCheck.id } })

        if (req.body.item_details && req.body.item_details.length > 0) {
            for( let val of req.body.item_details ){
                let parsed = typeof val === 'string' ? JSON.parse(val) : val;
                await item_detail.update({
                    item_id: queryCheck.id,
                    size: parsed.size,
                    price: parsed.price,
                    stock: parsed.stock,
                    discount: parsed.discount
                },{where : { id : parsed.id }})
            }
        }

        if (req.files.images && req.files.images.length > 0) {

            for(let i = 0 ; i < req.files.images.length; i++){

                let itemImagesQuery = await item_images.findOne({where: {
                    id: req.body.images_id[i]
                }})

                let checkFile = await existPromise(itemImagesQuery.image)

                if(checkFile){
                    await unlinkPromise(itemImagesQuery.image);
                }

                await itemImagesQuery.update({
                    item_id: queryCheck.id,
                    image_name: req.body.image_name[i],
                    image: "uploads/" + req.files.images[i].filename
                })

            }
        }

        if (req.body.tags && req.body.tags.length > 0) {

            let trimmed = req.body.tags.map((x,i) => {
                return x.trim().toLowerCase()
            });

            let tagFromDB = queryCheck.tags.map((x,i) => {
                return x.tag_name
            });

            await Promise.all(trimmed.map(async (x, i) => {

                let result = tagFromDB.indexOf(x);
                if(result < 0 ){
                    // create new tag if not exist
                    // first check the tag name
                    let queryTag = await tags.findOne({
                        where : {tag_name : x }
                    })

                    if(!queryTag){
                        // if not exist create new tag name and associate it
                        let insertTag = await tags.create({
                            tag_name: x,
                        })
                        await item_tags.create({
                            item_id: queryCheck.id,
                            tags_id: insertTag.id
                        })
                    }else{
                        // if exist, get the tag id and associate it with item
                        await item_tags.create({
                            item_id: queryCheck.id,
                            tags_id: queryTag.id
                        })
                    }
                }
            }));

            await Promise.all( tagFromDB.map(async (x,i) => {
                let result = trimmed.indexOf(x);

                if(result < 0 ){
                    // remove not exitsing tag 
                    // get the tag
                    let queryTag = await tags.findOne({
                        where : {tag_name : x }
                    })

                    // if exist, get the tag id and remove its relation in item_tags
                    await item_tags.destroy({
                        where : {
                            item_id : queryCheck.id,
                            tags_id : queryTag.id
                        }
                    })
                }
            }))
        }

        return res.status(200).json({
            success: true,
            message: "Item successfully updated"
        })

    } catch (error) {
        console.log(error);
        const status = {
            success: false,
            message: "Process failed",
            error : error.message
        }
        return res.status(400).json(status)
    }
}

let removeItem = async (req, res, next) => {
    try {
        if (!req.user.is_admin) {
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        }

        let queryCheck = await item.findOne({
            where: {
                id: req.params.itemId
            },
            include: [
                {model: item_images , as: 'item_images'},
                {model: item_detail, as : 'item_details'},
                {model: category, as: 'category'},
                {model: tags, as: 'tags'}
            ]
        })

        if (!queryCheck) {
            throw new Error('Item does not exist')
        }

        await item.destroy({
            where : {
                id : queryCheck.id
            }
        });

        let checkFile = await existPromise(queryCheck.featured_image)

        if(checkFile){
            await unlinkPromise(queryCheck.featured_image);
        }
        

        if (queryCheck.item_images && queryCheck.item_images.length > 0) {
            await Promise.all(queryCheck.item_images.map(async (x) => {
                let checkFileImage = await existPromise(x.image);
                if(checkFileImage){
                    await unlinkPromise(x.image);
                }
            }))
        }

        return res.status(200).json({
            success : true,
            message : "Item and its association has been removed"
        })

    } catch (error) {
        console.log(error);
        return res.status(400).send({
            success: false,
            message: error.message
        })
    }
}

let getItems = async (req, res, next) => {
    try {
        let condition;
        
        if(req.params.itemId){
            condition = {
                include: [
                    {model: item_images , as: 'item_images'},
                    {model: item_detail, as : 'item_details'},
                    {model: category, as: 'category'},
                    {model: tags, as: 'tags', through: {attributes: []}}
                ],
                where : {
                    id : req.params.itemId
                }
            };
        }else{
            condition = {
                include: []
            }

            if(Object.keys(req.query).length > 0){

                if(req.query.search){
                    let toArray = req.query.search.split(' ');
                    let like = toArray.map((x) => {
                        return { [Op.like] : `%${x}%` }
                    });
                    let result = like.map((x) => {
                        return {'item_name' : x}
                    })
                    condition.where = Object.assign({},{[Op.or] : result})
                }

                if(req.query.cat){
                    if(req.query.search){
                        condition.where = Object.assign(condition.where,{category_id : req.query.cat})
                    }else{
                        condition.where = Object.assign({},{category_id : req.query.cat})
                    }
                }

                if(req.query.tag){
                    let toArray = req.query.tag.split(',');
                    let result = toArray.map((x) => {
                        return { id: x }
                    });
                    let filterTag = { model: tags, as: 'tags', where: 
                        {[Op.or] :result},through: {attributes: []}
                        
                    }
                    condition.include.push(filterTag)

                }else{
                    let defaultTags = {model: tags, as: 'tags'};
                    condition.include.push(defaultTags)
                }

                if(req.query.limit){
                    condition.limit = req.query.limit ? req.query.limit : 10;
                }

                if(req.query.page){
                    let limit = req.query.limit ? req.query.limit : 0;
                    condition.offset = (req.query.page * limit) - limit;
                }
            }
        }

        let queryCheck = await item.findAll(condition)

        return res.status(200).json({
            success : true,
            message : "Item retrieved successfully",
            data : queryCheck
        })
        
    } catch (error) {
        console.log(error);
        return res.status(400).send(error)
    }
}

let unlinkPromise = (path) => {
    return new Promise((resolve, reject) => {
        fs.unlink(`public/${path}`, (err) => {
            try {
                if (err) throw err.message;
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    })
}

let existPromise = (path) => {
    return new Promise((resolve,reject) => {
        let result = fs.existsSync(`public/${path}`);
        if(!result){
            reject('File is not exist')
        }
        resolve(result)
    })
}


module.exports = {
    insertItemDetails,
    getItems,
    updateItem,
    removeItem
}