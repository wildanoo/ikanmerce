const invoice = require('../server/models').invoice;
const item_detail = require('../server/models').item_detail;
const cart = require('../server/models').cart;
const shippingAddr = require('../server/models').shippingAddr;
const users = require('../server/models').user;
const nodemailer = require('nodemailer');
const CronJob = require('cron').CronJob;

const axios = require('axios');
const encode = require('nodejs-base64-encode');
const tokenPro = "Mid-server-NmcmEqOapu8mslpllZTsUWVS";
const tokenSandbox = "SB-Mid-server-0xFGNOfVz_j_dkYZGXS9BYiz"
const urlPro = "https://app.midtrans.com/snap/v1/transactions"
const urlSb = "https://app.sandbox.midtrans.com/snap/v1/transactions"
const AUTH_TOKEN = encode.encode(tokenSandbox, 'base64')


const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const item = require('../server/models').item;

axios.defaults.headers.common['Authorization'] = 'Basic ' + AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/json';

let invoiceCreate = async function (req, res) {
    const { cartNum, recipient_name, phone, address, city, province } = req.body
    try {
        if (!req.user.email) {
            return res.status(401).json({
                success: false,
                message: "undefined user"
            })
        }

        const checkCartOut = await cart.findAll({
            where: {
                user_id: req.user.id,
                cart_number: cartNum,
                status: false
            }
        })

        if (!checkCartOut) {
            throw new Error("cart not found/ status cart to invoice = true")
        }

        let testData = []
        for (let i = 0; checkCartOut.length > i; i++) {
            testData.push(checkCartOut[i].item_detail_id)
        }
        // console.log(testData);
        let dataItemDetails = []
        if (testData.length > 0) {
            await Promise.all(testData.map(async (x) => {
                const dataTest = await item_detail.findOne({
                    where: {
                        id: x
                    }
                })
                dataItemDetails.push(dataTest)
            }))
        }
        const dataItemDetail = await item_detail.findAll({
            where: {
                id: testData
            }
        })
        if (dataItemDetails === null) {
            throw new Error("Data Item Detail Not Found")
        }

        let dataPrice = []
        for (let i = 0; dataItemDetails.length > i; i++) {
            dataPrice.push(dataItemDetails[i].price)
        }

        let dataStock = []
        for (let i = 0; dataItemDetails.length > i; i++) {
            dataStock.push(dataItemDetails[i].stock)
        }

        let dataDiscount = []
        for (let i = 0; dataItemDetails.length > i; i++) {
            dataDiscount.push((dataItemDetails[i].discount) / 100)
        }

        let dataPriceDiscount = []
        for (let i = 0; dataItemDetails.length > i; i++) {
            dataPriceDiscount.push(dataPrice[i] - (dataDiscount[i] * dataPrice[i]))
        }

        let dataQuantity = []
        for (let i = 0; checkCartOut.length > i; i++) {
            dataQuantity.push(checkCartOut[i].quantity)
        }

        let dataCurrentStock = []
        for (let i = 0; checkCartOut.length > i; i++) {
            dataCurrentStock.push(dataStock[i] - dataQuantity[i])
        }

        let dataTotalPrice = []
        for (let i = 0; checkCartOut.length > i; i++) {
            dataTotalPrice.push((dataPriceDiscount[i] * dataQuantity[i]))
        }

        let getTotal = 0
        for (let i = 0; dataTotalPrice.length > i; i++) {
            getTotal += dataTotalPrice[i]
        }

        let statusGo = true;
        if (checkCartOut.length > 0) {
            await Promise.all(checkCartOut.map(async (x) => {
                await x.update({
                    status: statusGo
                })
            }))

            for (let i = 0; i < dataCurrentStock.length; i++) {
                let stock = dataCurrentStock[i]
                await dataItemDetails[i].update({ stock: stock })
            }
        }

        if (getTotal === 0) {
            throw new Error("cant create invoice if total price = 0")
        }

        let createInvoice = await invoice.create({
            user_id: req.user.id,
            cart_number: cartNum,
            total_price: getTotal
        })

        if (!createInvoice) {
            throw new Error("failed to create Invoice")
        }

        let shippingAddrCreate = await shippingAddr.create({
            invoice_id: createInvoice.id,
            recipient_name,
            phone,
            address,
            city,
            province
        })
        // let shippingAddrCreate = await shippingAddr.create({
        //     invoice_id: createInvoice.id,
        //     namaPenerima,
        //     phone,
        //     alamat,
        //     kota,
        //     provinsi
        // })

        if (!shippingAddrCreate) {
            throw new Error("failed to add shipping address table")
        }

        let dataUserInvoice = await invoice.findOne({
            where: {
                user_id: req.user.id,
                id: createInvoice.id,
                status: true
            }
        })

        if (!dataUserInvoice) {
            throw new Error("failed to find data user invoice")
        }

        let time = require('../config/time')

        if (dataUserInvoice.total_price === 0) {
            throw new Error("total price = 0")
        }

        let response = await axios({
            method: 'post',
            url: urlSb,
            data: {
                "transaction_details": {
                    "order_id": `ORDER-${dataUserInvoice.id}`,
                    "gross_amount": dataUserInvoice.total_price
                },
                "customer_details": {
                    "first_name": req.user.name,
                    "email": req.user.email,
                    "phone": req.user.phone
                    // "billing_address": {
                    //     "recipient_name": recipient_name,
                    //     "phone": phone,
                    //     "alamat": address,
                    //     "kota": city,
                    //     "provinsi": province
                    // }
                },
                "expiry": {
                    "start_time": time.time(dataUserInvoice.createdAt),
                    "unit": "day",
                    "duration": 5
                }
            }
        })
        
        if (!response) {
            throw new Error("failed post request to midtrans")
        } else {
            await dataUserInvoice.update({
                payment_link: response.data.redirect_url
            })
        }

        let transporter = await nodemailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            auth: {
                user: "456c664af2e8d4",
                pass: "c9d25036ce1a59"
            }
        })

        let job = new CronJob({
            cronTime: `${time.timeCron(dataUserInvoice.createdAt)} * * 0-6`,
            onTick: function () {
                //cek database status v
                //if
                if (dataUserInvoice.status === true) {

                    let mailOptions = {
                        from: '"Para Pencari Ikan" <panduakas@gmail.com>',
                        to: req.user.email,
                        subject: `Invoice Payment Ikan`,
                        text: `To complete the payment, click this following link ${response.data.redirect_url}`
                    }

                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            // return res.send(error)                                            
                            console.log(error);
                        }
                        console.log("message was sent!");
                        console.log(info);

                    })
                } else {
                    job.stop()
                }

            },
            start: true,
            timeZone: 'Asia/Jakarta'
        })

        return res.status(200).json({
            success: true,
            message: "Payment link sent via email",
            checkCartOut,
            dataItemDetails,
            createInvoice,
            dataUserInvoice
        })

    } catch (error) {
        return res.status(400).json({
            success: false,
            message: error.message,
        })
    }
}
let getStatusPayment = async function (req, res) {
    try {
        if (req.body) {
            console.log('ada');
        } else {
            console.log('tidak ada');
        }
    } catch (error) {
        console.log(error);
    }
    // try {
    //     if (!req.user.email) {
    //         return res.status(401).json({
    //             success: false,
    //             message: 'user invoice undefined'
    //         })
    //     } else {

    //         try {
    //             if (req.body.status_code === (502)) {
    //                 console.log(req.body);
    //                 // const statusInvoiceUpdate = await invoice.update({
    //                 //     status: false
    //                 // })
    //                 // res.status(200).json({

    //                 // })
    //             } else {
    //                 console.log(req.body);
    //                 // res.status(400).json({

    //                 // })
    //             }
    //         } catch (error) {
    //             console.log(error);
    //         }
    //     }
    // } catch (error) {
    //     console.log(error);
    // }
}

let getAllInvoice = async (req, res, next) => {
    try {
        if (!req.user.is_admin) {
            return res.status(400).json({
                success: false,
                message: "Admin Only"
            })
        }
        let query;

        if(req.params.invoiceId){
            query = await invoice.findOne({
                where : { id : req.params.invoiceId},
                include: [{
                        model: users,
                        attributes: ['id', 'name']
                    },
                    {
                        model: shippingAddr,
                        as: 'shipping_address'
                    },
                    {
                        model: cart,
                        where: {
                            user_id: {
                                [Op.col]: 'invoice.user_id'
                            },
                        },
                        include : [{
                            model : item_detail,
                            as : 'item_details',
                            include : [{
                                model : item
                            }]
                        }]
                    }
                ]
            });
        }else{

            let condition = {
                include: [{
                        model: users,
                        attributes: ['id', 'name']
                    },
                    {
                        model: cart,
                        where: {
                            user_id: {
                                [Op.col]: 'invoice.user_id'
                            },
                        }
                    }
                ]
            }

            if(Object.keys(req.query).length > 0){
                if(req.query.limit){
                    condition.limit = req.query.limit
                }
                if(req.query.page){
                    let limit = req.query.limit ? req.query.limit : 10
                    condition.offset = (limit * req.query.page) - limit
                }
                if(req.query.between){
                    let range = req.query.between.split(',');
                    let date1 = range[0].split('-')
                    let date2 = range[1].split('-')

                    condition.where = {
                        createdAt: {
                            [Op.between]: [
                                new Date(Date.UTC(parseInt(date1[0]), parseInt(date1[1]) - 1)), 
                                new Date(Date.UTC(parseInt(date2[0]), parseInt(date2[1])))
                            ]
                        }
                    }
                }
            }
            query = await invoice.findAll(condition);
        }

        return res.status(200).json({
            success: true,
            message: 'berhasil',
            data: query
        })
    } catch (error) {
        console.log(error);
        return res.status(400).send(error.message)
    }
}

module.exports = {
    invoiceCreate,
    getStatusPayment
}