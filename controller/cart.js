const cart = require('../server/models').cart;
const item = require('../server/models').item;
const item_detail = require('../server/models').item_detail;

let getAllCart = async function(req, res) {
    try {
        if(!req.user.email) {
            return res.status(401).json({
                succes: false,
                message: 'silahkan login untuk melihat cart'
            })
        } else {
            try {
                let viewCart = await cart.findAll({
                    where: {
                        user_id: req.user.id
                    },
                    include: [
                        {
                            model: item
                        },
                        {
                            model: item_detail
                        }
                    ]
                })
                if (viewCart === null) {
                    res.status(400).json({
                        succes: false,
                        message: "Cart not found"
                    })
                } else {
                    // for (let i=0; viewCart.length > i; i++) {
                    //     const viewItem = await item.findOne({
                    //         where: {
                    //             id: viewCart[i].item_id
                    //         }
                    //     })
                    //     viewCart[i].item_id.push(viewItem)
                    // }
                    // for (let i=0; viewCart.length > i; i++) {
                    //     const viewItemDetail = await item_detail.findOne({
                    //         where: {
                    //             id: viewCart[i].item_detail_id
                    //         }
                    //     })
                    //     viewCart[i].item_detail_id.push(viewItemDetail)
                    // }
                   
                    res.status(201).json({
                        succes: true,
                        message: "Here is your cart",
                        viewCart
                    })
                }
            } catch (error) {
                return res.status(400).json({
                    succes: false,
                    message: "Failed to read Cart table",
                    error
                })
            }
        }
    } catch (error) {
        return res.status(400).json({
            success: false,
            message: "Process login failed",
            error
        })
    }
}

let checkOut = async function(req, res) {
    try {
        if(!req.user.email) {
            return res.status(401).json({
                success: false,
                message: "login first to check out"
            })
        } else {
            try {
                const cartNumberx = await cart.findAll({
                    where: {
                        user_id: req.user.id
                    }
                })

                let arrayku = []
                if (cartNumberx.length === 0) {
                    arrayku.push(0)
                } else {
                    for (let i=0; cartNumberx.length > i; i++) {
                        arrayku.push(cartNumberx[i].cart_number);
                    }
                }
                const cartNumberFinal = Math.max(...arrayku) + 1;

                if (req.body.length > 0) {
                    await req.body.map(async (x) => {
                        await cart.create({
                            user_id: req.user.id,
                            item_id: x.item_id,
                            item_detail_id: x.item_detail_id,
                            quantity: x.quantity,
                            cart_number: cartNumberFinal
                        })
                    })
                    return res.json({
                        success: true,
                        message: 'Create a new Cart',
                        cart_number: cartNumberFinal
                    })
                } else {
                    res.status(400).json({
                        success: false,
                        message: "Failed to add a New Cart"
                    })
                }
            } catch (error) {
                res.status(400).json({
                    success: false,
                    message: "failed to read tabel cart",
                    error
                })
            }
        }
    } catch (error) {
        res.status(400).json({
            success: false,
            message: "authentication failed",
            error
        })
    }
}

let deleteCart = async function(req, res) {
    try {
        if(!req.user.email) {
            return res.status(401).json({
                success: false,
                message: "login first to add items to cart"
            })
        } else {
           try {
            //    const dataCart = await cart.destroy({
            //        where: {
            //            user_id: req.user.id,
            //            cart_number: req.params.cartNum
            //        }
            //    })
            //    return res.json({
            //        dataCart,
            //        message: "success"
            //    })
               const dataCart = await cart.findAll({
                   where: {
                       user_id: req.user.id,
                       cart_number: req.params.cartNum
                   }
               })
               if (dataCart === null) {
                   return res.send('Cart number does not exist')
               } else {
                   for (let i=0; dataCart.length > i; i++) {
                    dataCart[i].destroy()
                   }
                    return res.send('Cart number: '+ req.params.cartNum + ' successfully deleted')
               }
           } catch (error) {
               res.status(400).json({
                   success: false,
                   message: "failed to read tabel cart"
               })
           }
        }
    } catch (error) {
        res.status(400).json({
            success: false,
            message: "authentication failed"
        })
    }
}

module.exports = {
    getAllCart,
    checkOut,
    deleteCart
}