const category = require('../server/models').category;
const fs = require('fs');
const {promisify} = require('util');

let getAll = async (req,res,next) => {
    try {
        let queryCheck;
        if(req.params.categoryId){
            queryCheck = await category.findOne({
                where : {id: req.params.categoryId}
            })
        }else{
            queryCheck = await category.findAll()
        }

        if(!queryCheck){
            return res.status(400).json({
                success: false,
                message: "Failed to retrieve data"
            })
        }

        return res.status(200).json({
            success : true,
            message : "Category retrieved successfully",
            data : queryCheck
        })

    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: "Process failed"
        })
    }
}

let create = async (req, res, next) => {
    try {
        if(!req.user.is_admin){
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        }

        let queryCheck = await category.findOne({
            where : {category_name: req.body.category_name.trim().toLowerCase()}
        })

        if(queryCheck){
            return res.status(400).json({
                success: false,
                message: "Category name already exist"
            })
        }

        if(!req.files){
            return res.status(400).json({
                success: false,
                message: "Category image is empty"
            })
        }

        // console.log('AAAAAAA : ',req.files);

        let createCategory = await category.create({
            category_name : req.body.category_name.trim().toLowerCase(),
            category_image : "uploads/"+req.files.category_image[0].filename
        });

        if(!createCategory){
            return res.status(400).json({
                success: false,
                message: "Failed to create category"
            })
        }

        return res.status(201).json({
            success : true,
            message : "Category saved successfully"
        })

    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: "Input process failed"
        })
    }
}

let remove = async (req, res, next) => {
    try {
        if(!req.user.is_admin){
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        }

        let queryCheck = await category.findOne({
            where : {id: req.params.categoryId}
        })

        if(!queryCheck){
            throw new Error("Category name not exist")
        }

        await queryCheck.destroy();

        let checkFile = await existPromise(queryCheck.category_image)

        if(checkFile){
            await unlinkPromise(queryCheck.category_image);
        }

        return res.status(200).json({
            success : true,
            message : "Category deleted"
        })

    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: error.message
        })
    }
}

let update = async (req, res, next) => {
    try {
        if(!req.user.is_admin){
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        }

        let queryCheck = await category.findOne({
            where : {id: req.params.categoryId}
        })

        if(!queryCheck){
            return res.status(400).json({
                success: false,
                message: "Category name not exist"
            })
        }

        let fileLength = Object.keys(req.files).length;
        
        let data = {
            category_name: req.body.category_name.trim().toLowerCase(),
            category_image : fileLength > 0 ? "uploads/"+req.files.category_image[0].filename : queryCheck.category_image
        }


        if(fileLength > 0){
            let checkFile = await existPromise(queryCheck.category_image)

            if(checkFile){
                await unlinkPromise(queryCheck.category_image);
            }
        }
        
        await queryCheck.update(data);

        return res.status(200).json({
            success : true,
            message : "Category updated"
        })

    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: "Input process failed, error"
        })
    }
}

let unlinkPromise = (path) => {
    return new Promise((resolve, reject) => {
        fs.unlink(`public/${path}`, (err) => {
            try {
                if (err) throw err.message;
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    })
}

let existPromise = (path) => {
    return new Promise((resolve,reject) => {
        let result = fs.existsSync(`public/${path}`);
        if(!result){
            reject('File is not exist')
        }
        resolve(result)
    })
}

module.exports = {
    getAll,
    create,
    remove,
    update
};