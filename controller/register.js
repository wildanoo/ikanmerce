var express = require('express');
var router = express.Router();
const user = require('../server/models').user;
const expressValidator = require('express-validator');
router.use(expressValidator());
const bcrypt = require('bcrypt')

let register = async function (req, res) {
    const { name, email, phone, address, gender, password, password2 } = req.body
      try {
        const dataUser = await user.findOne({
          where: {
            email: email,
            phone: phone
          }
        })
        if (!dataUser) {
          if (password === password2) {
            try {
              const newPassword = await bcrypt.hash(password, 10)
              const userCreate = await user.create({
                name: name,
                email: email,
                phone: phone,
                address: address,
                gender: gender,
                password: newPassword
              })
              return res.json({
                success : true,
                message : "Registration success"
              })
            } catch (error) {
              res.send(error.message)
            }
          } else {
            resultPassErr = 'Password confirmation does not match!'
            return res.send(resultPassErr)
          }
        } else {
          resultUserErr = 'Email or Phone number already exist!'
          return res.send(resultUserErr)
        }
      } catch (error) {
        res.send(error)
      }
  
    }

  module.exports = {register};