const tagsModel = require('../server/models').tags;

let getAll = async (req,res,next) => {
    try {
        let queryCheck = await tagsModel.findAll();
        if (!queryCheck) {
            return res.status(401).json({
                success: false,
                message: "Failed to retrieve data"
            })
        } else {
            return res.status(200).json({
                success : true,
                message : "Tags retrieved successfully",
                data : queryCheck
            })
        }
    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: "Process failed"
        })
    }
}

let addTags = async function (req, res) {
    try {
        const { tagItems } = req.body
        if(!req.user.is_admin){
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        } 
            
        const dataTags = await tagsModel.findOne({
            where: {
                tag_name: tagItems.trim().toLowerCase() 
            }
        })

        if (dataTags === null) {
            const tagsCreate = await tagsModel.create({
                tag_name: tagItems.trim().toLowerCase()
            })
            return res.status(201).json(tagsCreate)
        } else {
            return res.status(400).send('Tags already exist')
        }
        
    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: "Failed to check user is admin or not"
        })
    }
}

let updateTags = async function (req, res) {
    try {
        const {tagItems} = req.body
        if(!req.user.is_admin){
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        } 
        const dataTags = await tagsModel.findOne({
            where: {
                id: req.params.tagsId   
            }
        })
        if (dataTags.id === parseInt(req.params.tagsId)) {
            const tagsUpdate = await dataTags.update({
                tag_name: tagItems.trim().toLowerCase()
            })
            return res.status(200).json(tagsUpdate)
        } else {
            return res.status(400).send('Tags ID is not exist')
        }

    } catch (error) {
        console.log(error.message);
        return res.status(400).json({
            success: false,
            message: "Failed to check user is admin or not"
        })
    }
}

let removeTags = async function (req, res) {
    try {
        if(!req.user.is_admin){
            return res.status(401).json({
                success: false,
                message: "User is not an admin"
            })
        } 

        const dataTags = await tagsModel.findOne({
            where: {
                id: req.params.tagsId   
            }
        })
        if (dataTags) {
            await dataTags.destroy()
            return res.status(200).send('Tags successfully deleted')
        } else {
            return res.status(400).send('Tags ID is not exist')
        }
        
    } catch (error) {
        console.log(error.message);
        return res.status(400).send(error).json({
            success: false,
            message: "Failed to check user is admin or not"
        })
    }
}

module.exports = {
    getAll,
    addTags,
    updateTags,
    removeTags
};